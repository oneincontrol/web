---
title: "About"
date: 2023-11-15T16:09:43+05:30
draft: false
scrolltotop: false
custom_css: 
    - "scss/about.scss"
---

```markdown
Hello, friend. Hello, friend? That's lame.
Maybe I should give you a name, but that's a slippery slope. 
```

### grep /$(id -u -n): /etc/passwd

At present, I work as a Senior DevOps engineer, specializing in the Automation team.
My main focus is on leveraging python, ansible and kubernetes to streamline deployments, ensuring clients experience efficient and worry-free processes.
My passion for technology constantly fuels my desire to learn and stay up-to-date with the latest industry trends. I enjoy exploring new technologies and expanding my knowledge base.
DevOs, InfoSec, and privacy are particular areas of interest to me, as I value the importance of safeguarding information in our digital world.
I am excited to continue my journey in the ever-evolving landscape of technology, contributing to innovative projects and making a positive impact in the world of DevOps engineering.


