---
title: "/uses/"
date: "2024-02-24"
updated: "2024-02-24"
slug: "uses"
---

This is a "uses" page, which details what I use on a daily basis in my work as
a developer and general computing, as well as other tools, utilities and
resources that I find handy and wish to share with others. The concept was
popularized by [uses.tech](https://uses.tech).

> Last updated: Saturday, 24 February 2024

### lsof -u $(id -u -n)

1. **[Hardware](#hardware)**
2. **[Softare](#software)**

## Hardware <a name="hardware"></a>
### :desktop_computer: Desktop
* OS: Arch Linux
* CPU: AMD Ryzen 5 3600
* GPU: NVIDIA GeForce GTX 1660 SUPER
* RAM: 32GB Kingston Fury
* SSD: 1 TB Kingston NVMe M.2
* SSD: 240 GB Kingston
* Monitor: AOC Q27G2S 2K@165hz
* Monitor: Zowie BenQ 1080p@75hz

### :keyboard: Peripherals
* Headset : SteelSeries Arctis 7+
* Keyboard : Keychron 8 Pro TKL (Red Gateron Pro Switch)
* Mousepad : ZOWIE by BenQ G-SR-SE Rogue
* Mouse : Ninjutso Origin One X

### :iphone: Phone
* iPhone 14 Pro Max

### :books: Book reader
* Amazon Kindle Paperwhite

### :watch: Watch
* Amazfit Balance
* Amazfit T-Rex 2

## Software <a name="software"></a>



