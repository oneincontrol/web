# Use an official Hugo image as the base image
FROM razonyang/hugo:exts as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the contents of your Hugo site (including the Hermit theme) into the container
COPY . .

# Build the Hugo site
#RUN apk --no-cache add git
RUN hugo 

# Use a lightweight HTTP server to serve the site
FROM nginxinc/nginx-unprivileged:stable-alpine

# Copy the built site from the builder stage to the nginx web root
COPY --from=builder /app/public /usr/share/nginx/html

# Expose port 8080 for the web server
EXPOSE 8080

# Command to start the web server
CMD ["nginx", "-g", "daemon off;"]